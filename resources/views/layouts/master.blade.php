

<!doctype html>

<html lang="zxx" class="no-js">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="keywords" content="diseño web, marketing, pagina, autoadministrable, adwords, diseño grafico, diseño de paginas "/>
      <meta name="description" content="En Thorma Web Diseñamos el sitio web que necesitás, contamos con la experiencia y los recursos para hacer realidad lo que tienes en mente. consultanos." />
      <meta name="author" content="
      ___________.__                                   __      __      ___.    
      \__    ___/|  |__   ___________  _____ _____    /  \    /  \ ____\_ |__  
        |    |   |  |  \ /  _ \_  __ \/     \\__  \   \   \/\/   // __ \| __ \ 
        |    |   |   Y  (  <_> )  | \/  Y Y  \/ __ \_  \        /\  ___/| \_\ \
        |____|   |___|  /\____/|__|  |__|_|  (____  /   \__/\  /  \___  >___  /
                      \/                   \/     \/         \/       \/    \/ 
      " />
      <title>Thorma Web</title>
      <link rel='stylesheet' href='/assets/css/bootstrap.4.1.1.min.css'  media='all'/>
      <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:500,700,800%7CWork+Sans:400,500,700" media="all">
      <link rel='stylesheet' href='/assets/css/style.css'  media='all'/>
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css"  media="all">
      <link rel="stylesheet" href="/assets/css/animate.css">
      <link rel="shortcut icon" href="#" type="image/x-icon">
   </head>
   <body class="image-template">
	<div class="preloader"><div class="loader-slice"></div></div>

      <header id="masthead" class="site-header align-items-center d-flex">
         <div class="container">
            <div class="row align-items-center">
               <div class="col h-100">

                  <div class="site-logo">
                     <a href="/" class="custom-logo-link" rel="home">
                        <img src="/assets/images/logowhite.png" alt="ThormaWeb" class="custom-logo light-logo"/>
                        <img src="/assets/images/logothormaweb.png" alt="ThormaWeb" class="custom-logo dark-logo"/>
                     </a>

                  </div>

               </div>
               <div class="col h-100">
                  <a href="#contact" class="float-right animate_btn redirect-btn dark-btn wow pulse"> CONTACTENOS</a>
               </div>
            </div>

         </div>

      </header>
      @include('components.slide')
      @include('components.nav')

@yield('section')

      <footer id="colophon" class="site-footer">
        <div class="container">
           <div class="row">
              <div class="col text-center">
                 <ul class="footer-social mb-4 list-unstyled">
                    <li><a href="javascript:;"><i class="fa fa-facebook wow flip" ></i></a></li>
                    <li><a href="javascript:;"><i class="fa fa-twitter wow flip"></i></a></li>
                    <li><a href="javascript:;"><i class="fa fa-instagram wow flip"></i></a></li>
                    <li><a href="javascript:;"><i class="fa fa-google-plus wow flip"></i></a></li>
                 </ul>

                 <div class="site-info">
                    <span>&copy; <?php echo date('Y')?> </span>
                 </div>
              </div>
           </div>
        </div>
     </footer>
     <script  src='/assets/js/jquery3.3.1.min.js'></script>
     <script src='/assets/js/bootstrap-popper.min.js'></script>
     <script  src='/assets/js/bootstrap.4.1.1.min.js'></script>
     <script src="/assets/js/jquery.simple-text-rotator.min.js"></script>
     <script  src='/assets/js/jquery.mixitup.min.js?ver=20151215'></script>
     <script  src='/assets/js/modernizr.min.js'></script>
     <script  src='/assets/js/main.js'></script>
     <!--Efect wow-->
     <script src="/assets/js/wow.min.js"></script>
              <script>
              new WOW().init();
              </script>
  </body>
</html>
