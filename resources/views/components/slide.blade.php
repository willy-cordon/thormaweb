<section id="banner" class="homepage-hero" style="background-image: url('/assets/images/code4.jpg');">
    <div class="banner-wrapper ">
       <div class="container">
          <div class="row ">
             <div class="col-lg-8 ">
                <div class="banner-inner">
                   <div class="banner-text burger">
                      <h1 class="text-rotator">Diseñamos Y Desarrollamos Productos Para <span class="rotate">Startups., Empresas., Negocios., Servicios., Logisticas.</span></h1>
                      <p>Diseñamos, desarrollamos e implementamos soluciones con resultados reales sobre plataformas digitales permitiendo a las empresas explotar todo su potencial en la web.</p>
                      <a href="#first-section" class="circle-icon bottom redirect-btn">
                         <img width="50" src="/assets/images/arrow-down.svg" alt="arrow-down">
                      </a>
                   </div>
                </div>
             </div>
          </div>
       </div>
   </div>
 </section>
