<nav id="fixed-nav" class="site-menu ">
    <div class="container">
       <div class="row w-100">
          <div class="col-md-12">
             <a href="javascript:;" class="site-menu-trigger" ><span></span></a><!-- Responsive Toggle -->
             <div id="primary-menu" class="menu">
                <ul class="nav menu ">
                   <li class=""><a class="nav-link" href="#services">Servicios</a></li>
                   <li class=""><a class="nav-link" href="#about">Nosotros</a></li>
                   <li class=""><a class="nav-link" href="#portfolio">Portfolio</a></li>
                   <li class=""><a class="nav-link" href="#gallery">Galeria</a></li>
                </ul>
             </div>
          </div>
      </div>
    </div>
 </nav>
