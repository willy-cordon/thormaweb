@extends('layouts.master')

@section('section')





<main class='ps-main-content' id='first-section'>
    <section id="services" class="ps-section burger border-bottom">
       <div class="container">
          <div class="row">
             <div class="col-lg-4">
                <div class="page-head mb-5">
                   <span class="page-caption wow bounceInLeft">Servicios</span>
                   <h2 class="mt-3 wow bounceInLeft">Potencia tu espacio</h2>
                   <p>A lo largo de años nos hemos especializado en diseñar páginas web y tiendas online para negocios que quieren dar el salto al mundo digital de la mejor forma posible.
                     </p>
                     <p>
                         Nuestra experiencia nos ha llevado a entender muy bien las necesidades de comunicación y marketing de sectores como la consultoría, restauración, moda, turismo, logística, marketing, finanzas, retail o los recursos humanos.
                     </p>
                </div>
             </div>
             <div class="col-lg-8 service-list">
                <div class="row">
                   <div class="col-sm-6 service-item">
                      <div class="card">
                         <div class="card-content">
                            <div class="card-icon">
                               <img src="/assets/images/004-draw.svg" class="img-fluid wow bounceInLeft" alt="service">
                            </div>
                            <h3 class="title mt-3">Diseño web</h3>
                            <div class="post">
                               <p>Desarrollamos sitios profesionales a la medida de tus necesidades, en html y css, etc. Autoadminnistrables. Desarrollados principalmente en "Laravel".</p>
                            </div>
                         </div>
                      </div>
                   </div>
                   <div class="col-sm-6 service-item ">
                      <div class="card">
                         <div class="card-content">
                            <div class="card-icon">
                               <img src="/assets/images/002-smartphone.svg" class="img-fluid wow bounceInLeft" alt="service">
                            </div>
                            <h3 class="title mt-3"> UX/UI </h3>
                            <div class="post">
                               <p>Optimizamos cada sitio para maximizar la experiencia de sus usuarios, analizamos el comportamiento de sus clientes y mejoramos interacción.</p>
                            </div>
                         </div>
                      </div>
                   </div>
                   <div class="col-sm-6 service-item ">
                      <div class="card">
                         <div class="card-content">
                            <div class="card-icon">
                               <img src="/assets/images/001-arroba.svg" class="img-fluid wow bounceInLeft" alt="service">
                            </div>
                            <h3 class="title mt-3"> Marketing Digital </h3>
                            <div class="post">
                               <p>Campañas de posicionamiento web mediante estrategias SEO, Google Adwords, Facebook Ads., envío masivo de mails y gestión de redes sociales.</p>
                            </div>
                         </div>
                      </div>
                   </div>
                   <div class="col-sm-6 service-item ">
                      <div class="card">
                         <div class="card-content">
                            <div class="card-icon">
                               <img src="/assets/images/003-settings.svg" class="img-fluid wow bounceInLeft" alt="service">
                            </div>
                            <h3 class="title mt-3"> Extras </h3>
                            <div class="post">
                               <p>Realizamos un estudio de su rubro en el mercado y lo asesoramos según las necesidades de los tiempos que corren y sus preferencias.</p>
                            </div>
                         </div>
                      </div>
                   </div>
                </div>
             </div>
          </div>
       </div>
    </section>

    <section id="about" class="ps-section burger paral paralsec">
            <div class="container">
               <div class="row">
                  <div class="col-lg-12">
                     <div class="page-head mb-5">
                        <span class="page-caption wow bounceInLeft">Nosotros</span>
                        <h2 class="mt-3 wow bounceInLeft">Conocenos un poco más </h2>
                        <p>Somos una agencia con mas de 10 años de experiencia, nuestro principal objetivos es colaborar con nuestros clientes en alcanzar sus objetivos comerciales.</p>
                            <p>Diseñamos, desarrollamos e implementamos soluciones con resultados reales sobre plataformas digitales permitiendo a las empresas explotar todo su potencial en la web.</p>
                        <p>En pocas palabras, somos creadores, diseñadores, ingenieros, educadores y solucionadores de problemas.</p>
                     </div>
                  </div>

               </div>
            </div>

         </section>

    <section id="portfolio" class="ps-section burger border-bottom">
       <div class="container">
          <div class="row flex-item-stretch">
             <div class="col-lg-4">
                <div class="page-head mb-5">
                   <span class="page-caption wow bounceInLeft">Portfolio</span>
                   <h2 class="mt-3 wow bounceInLeft">Trabajos Realizados</h2>

                    <p>Actualmente Internet es un medio de comunicación imprescindible. Esta realidad implica que no solo es necesario tener un sitio web,
                        sino que es fundamental tener un Diseño de página web profesional.</p>
                </div>
             </div>
             <div class="col-lg-8 wow flipInX">
                <div class="portfolio-list align-items-center d-flex flex-wrap">
                   <div class="portfolio-item  item col-md-4 col-sm-6 ux-ui-design">
                      <div class="portfolio-inner">
                         <div class="portfolio_img" style="background-image: url(/assets/images/fire7.png);"></div>
                         <div class="portfolio-content d-flex justify-content-center align-items-center">
                            <span class="port-title">Rio azul films</span>
                            <a href="#portfolioModal" class="link-item port-link" data-toggle="modal"></a>

                         </div>
                         <div class="d-none port_tag">UX/UI Design</div>
                         <div class="d-none port_content">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            <ul>
                               <li>Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur.</li>
                               <li>Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</li>
                               <li>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</li>
                            </ul>
                        <div class="col h-100 botonpag">

                           <a target="_blank" href="http://www.rioazulfilms.com/" class="float-right animate_btn redirect-btn dark-btn">Visitar Pagina</a>
                        </div>
                         </div>


                      </div>
                   </div>
                   <div class="portfolio-item  item col-md-4 col-sm-6 ux-ui-design ">
                    <div class="portfolio-inner">
                       <div class="portfolio_img" style="background-image: url(/assets/images/fire8.png);"></div>
                       <div class="portfolio-content d-flex justify-content-center align-items-center">
                          <span class="port-title">Modo de vida</span>
                          <a href="#portfolioModal" class="link-item port-link" data-toggle="modal"></a>

                       </div>
                       <div class="d-none port_tag">UX/UI Design</div>
                       <div class="d-none port_content">
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                          <ul>
                             <li>Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur.</li>
                             <li>Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</li>
                             <li>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</li>
                          </ul>
                      <div class="col h-100 botonpag">

                         <a target="_blank" href="https://www.mododevida.com/" class="float-right animate_btn redirect-btn dark-btn">Visitar Pagina</a>
                      </div>
                       </div>


                    </div>
                 </div>
                 <div class="portfolio-item  item col-md-4 col-sm-6 ux-ui-design ">
                    <div class="portfolio-inner">
                       <div class="portfolio_img" style="background-image: url(/assets/images/fire9.png);"></div>
                       <div class="portfolio-content d-flex justify-content-center align-items-center">
                          <span class="port-title">Valuar propiedades</span>
                          <a href="#portfolioModal" class="link-item port-link" data-toggle="modal"></a>

                       </div>
                       <div class="d-none port_tag">UX/UI Design</div>
                       <div class="d-none port_content">
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                          <ul>
                             <li>Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur.</li>
                             <li>Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</li>
                             <li>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</li>
                          </ul>
                      <div class="col h-100 botonpag">

                         <a target="_blank" href="https://valuarpropiedades.com/" class="float-right animate_btn redirect-btn dark-btn">Visitar Pagina</a>
                      </div>
                       </div>


                    </div>
                 </div>
                 <div class="portfolio-item  item col-md-4 col-sm-6 ux-ui-design ">
                    <div class="portfolio-inner">
                       <div class="portfolio_img" style="background-image: url(/assets/images/fire1.png);"></div>
                       <div class="portfolio-content d-flex justify-content-center align-items-center">
                          <span class="port-title">Prokey</span>
                          <a href="#portfolioModal" class="link-item port-link" data-toggle="modal"></a>

                       </div>
                       <div class="d-none port_tag">UX/UI Design</div>
                       <div class="d-none port_content">
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                          <ul>
                             <li>Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur.</li>
                             <li>Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</li>
                             <li>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</li>
                          </ul>
                      <div class="col h-100 botonpag">

                         <a target="_blank" href="https://prokey.com.ar/" class="float-right animate_btn redirect-btn dark-btn">Visitar Pagina</a>
                      </div>
                       </div>


                    </div>
                 </div>
                   <div class="portfolio-item  item col-md-4 col-sm-6 development ">
                      <div class="portfolio-inner">
                         <div class="portfolio_img" style="background-image: url(/assets/images/fire2.png);"></div>
                         <div class="portfolio-content d-flex justify-content-center align-items-center">
                            <span class="port-title">Gajat</span>
                            <a href="#portfolioModal" class="link-item port-link" data-toggle="modal"></a>

                         </div>
                         <div class="d-none port_tag">Development </div>
                         <div class="d-none port_content">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            <ul>
                               <li>Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur.</li>
                               <li>Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</li>
                               <li>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</li>
                            </ul>
                            <div class="col h-100 botonpag">

                             <a target="_blank" href="https://www.gajat.org.ar/" class="float-right animate_btn redirect-btn dark-btn">Visitar Pagina</a>
                          </div>
                         </div>
                      </div>
                   </div>
                   <div class="portfolio-item  item col-md-4 col-sm-6 iot ">
                      <div class="portfolio-inner">
                         <div class="portfolio_img" style="background-image: url(/assets/images/fire3.png);"></div>
                         <div class="portfolio-content d-flex justify-content-center align-items-center">
                            <span class="port-title">Cefir</span>
                            <a href="#portfolioModal" class="link-item port-link" data-toggle="modal"></a>

                         </div>
                         <div class="d-none port_tag">Internet Of Things</div>
                         <div class="d-none port_content">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            <ul>
                               <li>Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur.</li>
                               <li>Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</li>
                               <li>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</li>
                            </ul>
                            <div class="col h-100 botonpag">

                             <a target="_blank" href="http://www.cefir.com.ar/" class="float-right animate_btn redirect-btn dark-btn">Visitar Pagina</a>
                          </div>
                         </div>
                      </div>
                   </div>
                   <div class="portfolio-item  item col-md-4 col-sm-6 mobile-apps ">
                      <div class="portfolio-inner">
                         <div class="portfolio_img" style="background-image: url(/assets/images/fire4.png);"></div>
                         <div class="portfolio-content d-flex justify-content-center align-items-center">
                            <span class="port-title">Idriesshah foundation</span>
                            <a href="#portfolioModal" class="link-item port-link" data-toggle="modal"></a>

                         </div>
                         <div class="d-none port_tag">Mobile App</div>
                         <div class="d-none port_content">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            <ul>
                               <li>Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur.</li>
                               <li>Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</li>
                               <li>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</li>
                            </ul>
                            <div class="col h-100 botonpag">

                             <a target="_blank" href="https://idriesshahfoundation.org/" class="float-right animate_btn redirect-btn dark-btn">Visitar Pagina</a>
                          </div>
                         </div>
                      </div>
                   </div>
                   <div class="portfolio-item  item col-md-4 col-sm-6 ux-ui-design ">
                      <div class="portfolio-inner">
                         <div class="portfolio_img" style="background-image: url(/assets/images/fire5.png);"></div>
                         <div class="portfolio-content d-flex justify-content-center align-items-center">
                            <span class="port-title">hfittipaldi</span>
                            <a href="#portfolioModal" class="link-item port-link" data-toggle="modal"></a>

                         </div>
                         <div class="d-none port_tag">UX/UI Design </div>
                         <div class="d-none port_content">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            <ul>
                               <li>Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur.</li>
                               <li>Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</li>
                               <li>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</li>
                            </ul>
                            <div class="col h-100 botonpag">

                             <a target="_blank" href="http://www.hfittipaldi.com.ar/" class="float-right animate_btn redirect-btn dark-btn">Visitar Pagina</a>
                          </div>
                         </div>
                      </div>
                   </div>
                   <div class="portfolio-item  item col-md-4 col-sm-6 development ">
                      <div class="portfolio-inner">
                         <div class="portfolio_img" style="background-image: url(/assets/images/fire6.png);"></div>
                         <div class="portfolio-content d-flex justify-content-center align-items-center">
                            <span class="port-title">Apurabici</span>
                            <a href="#portfolioModal" class="link-item port-link" data-toggle="modal"></a>

                         </div>
                         <div class="d-none port_tag">Development</div>
                         <div class="d-none port_content">
                            <p> TG CONTENT Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                            <ul>
                               <li>Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur.</li>
                               <li>Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</li>
                               <li>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</li>
                            </ul>
                            <div class="col h-100 botonpag">

                             <a target="_blank" href="https://apurabici.com/es/" class="float-right animate_btn redirect-btn dark-btn">Visitar Pagina</a>
                          </div>
                         </div>
                      </div>
                   </div>
                </div>
             </div>
          </div>
       </div>
       <div  class="modal port_modal" id="portfolioModal"  role="dialog" >
          <a href="javascript:;" class="dismiss-item1 modal_close_btn" data-dismiss="modal" aria-label="Close"></a>
          <div class="row align-items-stretch port-popup slideshow flex-column flex-sm-row">
             <div class="col-lg-6 p-0">
                <div id="port_img" style="background-image: url(/assets/images/starbucks.jpg);"  ></div>
             </div>
             <div class="col-lg-6 p-0">
                <div class="d-flex f justify-content-stretch h-100">
                   <div class="content flex-column justify-content-center d-flex">

                      <h3 class="mt-3 text-capitalize">Starbucks</h3>
                      <small class="text-muted">UX/UI Design</small>
                      <div class="mt-3" id="port-content">
                         <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                         <ul>
                            <li>Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur.</li>
                            <li>Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</li>
                            <li>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</li>
                         </ul>
                      </div>
                   </div>
                </div>
             </div>
          </div>
       </div>
    </section>

     <div id="gallery" class="ps-section carousel slide carousel-fade " >
       <div class="carousel-inner" role="listbox">
          <div class="burger carousel-item active" style="background-image: url('/assets/images/1.jpg');">
             <div class="p-3 gallery-description   ">
                <h2 class="text-center">Diseño Atractivo</h2>
             </div>
          </div>
          <div class="burger carousel-item" style="background-image: url('/assets/images/2.jpg');">
             <div class="p-3 gallery-description   ">
                <h2 class="text-center">Paginas Autoadminnistrables</h2>
             </div>
          </div>

          <div class="burger carousel-item" style="background-image: url('/assets/images/di.jpg');">
             <div class="p-3 gallery-description   ">
                <h2 class="text-center">@ Quiero mi web</h2>
             </div>
          </div>
       </div>
       <div class="gallery-controls">
          <a class="carousel-control-prev " href="#gallery" role="button" data-slide="prev">
             <span class="" aria-hidden="true">
                <img src="/assets/images/back.svg" alt="">
             </span>
             <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next" href="#gallery" role="button" data-slide="next">
             <span class="" aria-hidden="true">
                <img src="/assets/images/next.svg" alt="">
             </span>
             <span class="sr-only">Next</span>
          </a>
       </div>
    </div>



    <section id="contact" class="t-burger ps-section">
       <div class="burger darkbg">
          <div class="container">
             <div class="row">
                <div class="col-lg-4">
                   <div class="page-head mb-5">
                      <span class="page-caption"> <h2 class="mt-3 wow bounceInLeft">Contactenos</h2></span>

                      <div class="contact-address">
                        <p>Nos interesa saber sus preguntas y comentarios.
                        Escribanos un mensaje y nos pondremos en contacto con usted cuanto antes.</p>


                      </div>
                   </div>
                </div>
                <div class="col-lg-8">
                   <form id="ajax-contact" method="post" action="/contact.php" class="form-contact d-flex justify-content-center">
                      <div id="form-messages" class="notification contact"></div>
                      <div class="row w-100">
                         <div class="col-md-6">
                            <div class="form-group">
                               <input type="text" required name="FullName" value="" size="40" class="form-control" id="name" aria-required="true" aria-invalid="false" placeholder="Nombre">
                            </div>
                            <div class="form-group">
                               <input type="email" required name="Email" value="" size="40" class="form-control" id="email" aria-required="true" aria-invalid="false" placeholder="Email">
                            </div>
                            <div class="form-group">
                               <input type="tel" name="Phone" required value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required form-control" id="phone" aria-required="true" aria-invalid="false" placeholder="Teléfono">
                            </div>
                         </div>
                         <div class="col-md-6 d-flex justify-content-between flex-column">
                            <div class="form-group">
                               <textarea name="Message" required cols="50" rows="4" class="wpcf7-form-control wpcf7-textarea form-control" id="message" aria-invalid="false" placeholder="Mensaje"></textarea>
                            </div>
                            <div class="form-group">
                               <input type="submit" value="Enviar" class="wpcf7-form-control wpcf7-submit dark-btn" id="submit">
                            </div>
                         </div>
                      </div>
                   </form>
                </div>
             </div>
          </div>
       </div>
    </section>
 </main>

@endsection
